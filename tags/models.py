from django.db import models


# Create your models here.
class Tag(models.Model):
    name = models.CharField(max_length=20)
    recipes = models.ManyToManyField("recipes.Recipe", related_name="tags")


# try:
# from recipes.forms import RecipeForm
# from recipes.models import Recipe
# from recipes.urls.py import RecipeCreateView
# from recipes.list import RecipeListView
#
#
# except Recipe.DoesNotExist:
# # return redirect("recipes_list")

# Test your fix
# To test your fix, you need to do the same steps, again, plus more.
#
# Open a tab that contains the recipe detail page
# Open a tab that contains the admin page for the recipe
# Delete the recipe from the admin
# Submit the rating form
