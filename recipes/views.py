from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

# Requiring logins from the view
# We want all of the create, update, and delete views in the application
# to require the person to login.
# In both of your views.py files, import the mixin that requires
# someone to login.

from recipes.forms import RatingForm
from recipes.forms import RecipeForm
from recipes.models import Recipe

# Then, you need to apply it to every create, update, and delete view.
# Here's an example of how that's done.

# class RecipeCreateView(LoginRequiredMixin, CreateView):
# model = Recipe
# template_name = "recipes/new.html"
# fields = ["name", "author", "description", "image"]
# success_url = reverse_lazy("recipes_list")


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            rating.recipe = Recipe.objects.get(pk=recipe_id)
            rating.save()
    return redirect("recipe_detail", pk=recipe_id)


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 2


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        return context

    def get_queryset(self):
        return Recipe.objects.filter(owner=self.request.user)


class RecipeCreateView(
    LoginRequiredMixin, CreateView
):  # Add LoginRequiredMixin
    # o create, update, and delete view
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    # Add LoginRequiredMixin
    # to create, update, and delete view
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "author", "description", "image"]
    success_url = reverse_lazy("recipes_list")


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    # Add LoginRequiredMixin
    # to create, update, and delete view
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")
