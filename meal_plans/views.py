from django.shortcuts import redirect, render  # Which one should I Use and Why?
from django.urls import (
    reverse_lazy,
)  # what is reverse_lazy and why do we need to import it?

# Normally the Url pattern is taking you to the Views, but the reason why its called reverse_lazy is
# so that reverse_lazy is taking you back to the URL pattern.
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

# Why is it generic.*** and not just django.views?
# what makes generic function so important? Generic is a django library that contains generic class views
# Create your views here.


from meal_plans.models import MealPlan

#  Then, you need to apply it to every create, update, and delete view.
#  Here's an example of how that's done.
#  class RecipeCreateView(LoginRequiredMixin, CreateView):
#  model = Recipe
#  template_name = "recipes/new.html"
#  fields = ["name", "author", "description", "image"]
#  success_url = reverse_lazy("recipes_list")


class MealPlanListView(ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"
    paginate_by = 2


# def get_queryset(self):
# return MealPlan.objects.filter(owner=self.request.user)
# The error is telling you that request.user is an anonymous user.
# That means that they are not logged in, so it doesn't make sense
# to try to filter with filter(user=request.user).
# The easiest fix is to use the login_required decorator, to make
# sure that only logged-in users can access the view.


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meal_plans/new.html"
    fields = ["name", "date", "owner", "recipes"]
    success_url: reverse_lazy("meal_plans_list")

    def form_valid(self, form):
        MealPlan = form.save(commit=False)
        MealPlan.instance.owner = self.request.user
        MealPlan.save()
        form.save_m2m()
        # self.request.user(in a class view. the self is a class instance.) or request.user(in a function based view)
        # when the user makes a request to the website.
        return redirect(
            "meal_plans_detail", pk=plan.id
        )  # super().form_valid(form)


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plan/delete.html"
    success_url = reverse_lazy("meal_plans_list")


# def get_queryset(self):
# return MealPlan.objects.filter(owner=self.request.user)
# The error is telling you that request.user is an anonymous user.
# That means that they are not logged in, so it doesn't make sense
# to try to filter with filter(user=request.user).
# The easiest fix is to use the login_required decorator, to make
# sure that only logged-in users can access the view.


class MealPlanDetailView(LoginRequiredMixin, DetailView):
    model = MealPlan
    template_name = "meal_plan/detail.html"
    # What does the code below means!!!!!

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        return context


class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "recipes/edit.html"
    fields = ["name", "date", "owner", "recipes"]
    success_url = reverse_lazy("meal_plans_list")


# def get_queryset(self):
# return MealPlan.objects.filter(owner=self.request.user)
# The error is telling you that request.user is an anonymous user.
# That means that they are not logged in, so it doesn't make sense
# to try to filter with filter(user=request.user).
# The easiest fix is to use the login_required decorator, to make
# sure that only logged-in users can access the view.
