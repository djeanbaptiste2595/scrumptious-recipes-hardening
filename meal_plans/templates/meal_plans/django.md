As you may recall, Django Web applications have two different kinds of pieces: one Django project and one or more Django apps. Those two things are really important to differentiate between:

The Django project is the thing that configures and controls the entire Web application
Each Django app is there to provide a place to put together the related HTML templates, models, views, URL configurations, and static assets
Create a Django project
Once we have that installed, we use the django-admin tool to create a Django project. Here we are at a moment of truth.
The Django project is the first thing created. It's the key to how the entire Django Web application will run.
It makes sense that we would create it first. We create the Django project with the startproject command for the django-admin utility.
We give it a project name, and optionally, the directory we want the project installed to.
When it's done running, it will have created a few files for us. There's our fancy manage.py that we can use to manage our Django Web application.

It also creates a new directory with the name of the project that we gave it. Inside that directory, we'd find four files:

__init__.py, a file used for Python to make the directory a loadable module, so not so much a Django thing, but more a Python thing that Django relies on.
asgi.py, a file used to start the Django Web application using a third-party asynchronous Web server.
settings.py, the collection of all the necessary settings for the Django application to happily run.
urls.py, the principal file that Django uses to configure the paths that it will match to when it receives an HTTP request.
wsgi.py, a file used to start the Django Web application using a third-party Web server, like gunicorn, the Web server we used to deploy our Django application.
